package Example_JavaNetworking;
//The clients traverse all documents in a given directory and look for all .txt and .csv files larger than 100 ?B,
//and smaller than 10 KB.
//
//When the server receives a message from a client, the message is saved in the clients_data.txt file,
//which exists locally at the server. Each message from each client is stored in a separate line in the same document,
// in the following format:
//
//<IP-address-of-the-client> <port-of-the-client> <number-of-files>
//
//You need to enable multiple clients to send data at the same time.
//
//The server listens at port 3398. The order of the messages from the clients is not important.


import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class SocketWorker extends Thread{
    private Socket socket;
    private BufferedWriter bw;

    public SocketWorker(Socket socket, BufferedWriter bw){
        this.socket = socket;
        this.bw = bw;

    }

    @Override
    public void run() {
        try {
            receiveData(this.socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    void receiveData(Socket socket) throws IOException {
        DataInputStream dis = new DataInputStream(socket.getInputStream());
        int numFiles = dis.readInt();
        //sync
        synchronized (SocketWorker.class) {
            bw.write(String.format("%s %d %d",
                    socket.getInetAddress().getHostAddress(),
                    socket.getPort(),
                    numFiles));
            bw.newLine();
            bw.flush();
        }
    }


}
