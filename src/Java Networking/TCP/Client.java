package Example_JavaNetworking;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.Socket;

public class Client extends Thread{
    String serverAddress;
    int port;
    String folderToSearch;

    public Client(String serverAddress, int port, String folderToSearch){
        this.serverAddress = serverAddress;
        this.port = port;
        this.folderToSearch = folderToSearch;
    }

    static int getFiles(String folderToSearch){
        File file = new File(folderToSearch);
        FilenameFilter txtCsv = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                String lowerCaseName = name.toLowerCase();
                if(lowerCaseName.endsWith(".txt") || lowerCaseName.endsWith(".csv"))
                    return true;
                else return false;
            }
        };
        File[] files = file.listFiles(txtCsv);
        int numFiles = 0;
        for (File f : files){
            if(f.isFile() && f.length() > 10 *  1024 && f.length() < 100 * 1024)//1024 = 1KB
                numFiles++;
        }
        return numFiles;
    }

    static void sendData(String serverAddress, int port, int numFiles) throws IOException {
        Socket socket = null;
        try {
            socket = new Socket(serverAddress, port);
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeInt(numFiles);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            socket.close();
        }

    }
    @Override
    public void run() {
        int numFiles = getFiles(this.folderToSearch);
        try {
            sendData(serverAddress,port,numFiles);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Client c1 = new Client("localhost",3398, "C:\\Users\\Jovana\\Desktop");
        Client c2 = new Client("localhost",3398, "C:\\Users\\Jovana\\OS");
        c1.start();
        c2.start();
    }
}
