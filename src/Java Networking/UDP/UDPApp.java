package Example_JavaNetworking;

public class UDPApp {

    public static void main(String[] args) {
        UDPServer server = new UDPServer(8080);
        server.start();
        UDPClient client = new UDPClient("localhost", 8080, "Hello world");
        client.start();
    }
}
