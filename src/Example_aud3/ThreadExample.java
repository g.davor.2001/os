package Examples;

public class ThreadExample {

    public static void main(String[] args) throws InterruptedException {
        SharedSpace space1 = new SharedSpace();        SharedSpace space = new SharedSpace();
        SharedSpace space2 = new SharedSpace();


        Thread t1 = new ThreadClass("T1", space1);
        Thread t2 = new Thread(new RunnableClass("T2",space2));

        t1.start();
        t1.join();
        System.out.println(space1.counter);
        t2.start();
        t2.join();
        System.out.println(space2.counter);
    }
}

class ThreadClass extends Thread{
    String name;
    SharedSpace space;

    ThreadClass(String name, SharedSpace space){
        this.name = name;
        this.space = space;
    }

    @Override
    public void run() {
        for(int i = 0; i<10; i++) {
            space.increment();
            System.out.println("Thread class");
        };

    }
}

class RunnableClass implements Runnable{
    String name;
    SharedSpace space;
    public  RunnableClass(String name, SharedSpace space){
        this.name = name;
        this.space = space;
    }
    @Override
    public void run() {
        for(int i = 0;i<10; i++) {
            space.increment();
            System.out.println("Runnable class");
        }

    }
}

class SharedSpace{
    static int counter = 0;
    static void increment(){
        counter++;
    }

    int getCounter(){
        return counter;
    }
}
