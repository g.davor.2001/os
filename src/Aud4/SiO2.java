package Aud4;

import javax.swing.text.StyleContext;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SiO2 {

    static Semaphore si = new Semaphore(1);
    static Semaphore o = new Semaphore(2);
    static Semaphore siHere = new Semaphore(0);
    static Semaphore oHere = new Semaphore(0);
    static Semaphore ready = new Semaphore(0);


    static class Si extends Thread{
        public void bond(){
            System.out.println("Si is bonding now...");


        }

        public void execute() throws InterruptedException {
            si.acquire();
            siHere.release(2); //1 2
            oHere.acquire(2);// 1 2
            ready.release(2);
            bond();
            si.release();;
        }

        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class O extends Thread{
        public void bond(){
            System.out.println("O is bonding now....");
        }

        public  void execute() throws InterruptedException {
            o.acquire();
            siHere.acquire();//1: checked 2:
            oHere.release();//1: checked 2:
            ready.acquire();
            bond();
            o.release();
        }

        @Override
        public void run() {
            try {
                execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args) throws InterruptedException {
        List<Si> siList = new ArrayList<>();
        List<O> oList = new ArrayList<>();
        for(int i = 0; i < 5; i++)
            siList.add(new Si());
        for(int i = 0; i<10; i++)
            oList.add(new O());

        for(Si si: siList)
            si.start();
        for(O o: oList)
            o.start();

        for(Si si: siList)
            si.join(2000);
        for(O o: oList)
            o.join(2000);

    }
}
