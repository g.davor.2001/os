package Examples;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class BlockingSemaphoreQueue<T> {
    List<T> products = new ArrayList<>();
    int capacity;
    Semaphore producerSemaphore = new Semaphore(100);
    Semaphore consumerSemaphore = new Semaphore(100);

    Semaphore coordinator = new Semaphore(1);
    public BlockingSemaphoreQueue(int capacity){
        this.capacity = capacity;
    }

    //adding product from given producer
    public  void enqueue(T item) throws InterruptedException {
        //give key to the new producer
        producerSemaphore.acquire();//1 2 3.....100 1 ... 2....100

        coordinator.acquire();

        while(products.size() == capacity){
            coordinator.release();
            Thread.sleep(1000);
            coordinator.acquire();
        }

        products.add(item);
        coordinator.release();
        consumerSemaphore.release();

    }

    public synchronized T dequeue() throws InterruptedException {
        T item = null;
        //give key to a given costumer
        consumerSemaphore.acquire();//1 ..... 100

        coordinator.acquire();
        while (products.size() == 0){
            coordinator.release();
            Thread.sleep(1000);
            coordinator.acquire();
        }
        item = products.remove(products.size() - 1);
        coordinator.release();
        producerSemaphore.release();
        return item;
    }
}
